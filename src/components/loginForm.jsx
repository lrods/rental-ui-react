import React from "react";
import Joi from "joi-browser";
import Form from "./common/form";
import { baseUrl } from "../config.json";
import http from "../services/httpService";
import { notify } from "./../utils/notify";

class LoginForm extends Form {
  state = {
    data: { username: "", password: "" },
    errors: {}
  };

  schema = {
    username: Joi.string()
      .email()
      .required()
      .label("Username"),
    password: Joi.string()
      .min(6)
      .required()
      .label("Password")
  };

  doSubmit = async () => {
    try {
      const { data } = await http.post(baseUrl + "/auth", {
        email: this.state.data.username,
        password: this.state.data.password
      });

      localStorage.setItem("token", data);
      window.location = "/";
    } catch (error) {
      notify("error", "Incorrect credentials or deleted account");
    }
  };

  render() {
    return (
      <div>
        <h1>Login page!!!</h1>

        <form onSubmit={this.handleSubmit}>
          {this.renderInput("username", "Email Address", "email")}
          {this.renderInput("password", "Password", "password")}
          {this.renderButton("Submit")}
        </form>
      </div>
    );
  }
}

export default LoginForm;
