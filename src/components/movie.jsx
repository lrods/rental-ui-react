import React, { Component } from "react";
import MoviesTable from "./moviesTable";
import Pagination from "./pagination";
import { paginate } from "../utils/paginate";
import http from "../services/httpService";
import ListGroup from "./listgroup";
import _ from "lodash";
import { Link } from "react-router-dom";
import { baseUrl } from "../config.json";

class Movie extends Component {
  state = {
    moviesList: [],
    pageSize: 4,
    currentPage: 1,
    genres: [],
    sortColumn: { path: "title", order: "asc" }
  };

  async componentDidMount() {
    try {
      const { data } = await http.get(baseUrl + "/genres");
      const genres = [{ _id: "", name: "All genres" }, ...data];
      const { data: movies } = await http.get(baseUrl + "/movies");
      const moviesList = movies;
      this.setState({ genres, moviesList });
    } catch (error) {
      console.log(error);
    }
  }

  render() {
    const { length: count } = this.state.moviesList;

    if (count === 0) return <p>There are no movies in the database</p>;

    const filteredMovie =
      this.state.selectedGenre && this.state.selectedGenre._id
        ? this.state.moviesList.filter(
            movie => movie.genre._id === this.state.selectedGenre._id
          )
        : this.state.moviesList;

    const sorted = _.orderBy(
      filteredMovie,
      [this.state.sortColumn.path],
      [this.state.sortColumn.order]
    );

    const movies = paginate(
      sorted,
      this.state.currentPage,
      this.state.pageSize
    );

    return (
      <div className="row mt-2">
        <div className="col-3">
          <ListGroup
            items={this.state.genres}
            onClick={this.handleGenre}
            selectedItem={this.state.selectedGenre}
          />
        </div>
        <div className="col">
          <Link className="btn btn-primary" to="/movies/new">
            New movie
          </Link>
          <p>There are {filteredMovie.length} movies in the database</p>

          <MoviesTable
            movies={movies}
            onLike={this.handleLike}
            onDelete={this.handleDelete}
            onSort={this.handleSort}
          />
          <Pagination
            itemCount={filteredMovie.length}
            pageSize={this.state.pageSize}
            currentPage={this.state.currentPage}
            onPageChange={this.handlePageChange}
          />
        </div>
      </div>
    );
  }

  handleDelete = async id => {
    const originalMoviesList = [...this.state.moviesList];
    let movies = originalMoviesList.filter(movie => {
      return movie._id !== id;
    });

    this.setState({ moviesList: movies });

    try {
      await http.delete(baseUrl + "/movies/" + id);
    } catch (error) {
      console.log(error);
      this.setState({ moviesList: originalMoviesList });
    }
  };

  handleLike = movie => {
    const movies = [...this.state.moviesList];
    const index = movies.indexOf(movie);
    movies[index] = { ...movies[index] };
    movies[index].liked = !movies[index].liked;
    this.setState({ moviesList: movies });
  };

  handlePageChange = page => {
    this.setState({ currentPage: page });
  };

  handleGenre = genre => {
    this.setState({ selectedGenre: genre, currentPage: 1 });
  };

  handleSort = path => {
    const sortColumn = { ...this.state.sortColumn };
    if (sortColumn.path === path)
      sortColumn.order = sortColumn.order === "asc" ? "desc" : "asc";
    else {
      sortColumn.path = path;
      sortColumn.order = "asc";
    }
    this.setState({ sortColumn: sortColumn });
  };
}

export default Movie;
