import React, { Component } from "react";
import Joi from "joi-browser";
import Input from "./input";
import Select from "./select";

class Form extends Component {
  state = { data: {}, errors: {} };

  handleChange = ({ currentTarget }) => {
    const data = { ...this.state.data };
    const errors = { ...this.state.errors };

    data[currentTarget.name] = currentTarget.value;

    const result = Joi.validate(
      currentTarget.value,
      this.schema[currentTarget.name]
    );

    if (result.error) errors[currentTarget.name] = result.error.message;
    else delete errors[currentTarget.name];

    this.setState({ data, errors });
  };

  validate = () => {
    const result = Joi.validate(this.state.data, this.schema, {
      abortEarly: false
    });

    if (!result.error) return null;

    const errors = {};

    result.error.details.forEach(element => {
      errors[element.path[0]] = element.message;
    });

    return errors;
  };

  handleSubmit = event => {
    event.preventDefault();

    const res = this.validate();

    this.setState({ errors: res || {} });

    if (res) return;

    this.doSubmit();
  };

  renderButton(label) {
    return (
      <button
        disabled={this.validate()}
        type="submit"
        className="btn btn-primary"
      >
        {label}
      </button>
    );
  }

  renderInput(name, label, type) {
    return (
      <Input
        name={name}
        label={label}
        value={this.state.data[name]}
        type={type}
        onChange={this.handleChange}
        errors={this.state.errors[name]}
      />
    );
  }

  renderSelect(name, label, optionItems) {
    return (
      <Select
        name={name}
        label={label}
        options={optionItems}
        value={this.state.data[name]}
        onChange={this.handleChange}
        errors={this.state.errors[name]}
      />
    );
  }
}

export default Form;
