import React from "react";

const Input = ({ name, label, value, type, onChange, errors }) => {
  return (
    <div className="form-group">
      <label htmlFor={name}>{label}</label>
      <input
        value={value}
        onChange={onChange}
        type={type}
        className="form-control"
        id={name}
        name={name}
      />

      {errors && <div className="alert alert-danger">{errors}</div>}
    </div>
  );
};

export default Input;
