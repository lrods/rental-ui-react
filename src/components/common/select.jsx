import React from "react";

const Select = ({ name, label, value, options, errors, onChange }) => {
  return (
    <div className="form-group">
      <label htmlFor={name}>{label}</label>
      <select
        value={value}
        onChange={onChange}
        className="form-control"
        id={name}
        name={name}
      >
        <option value="">Please select an option</option>
        {options.map(item => (
          <option key={item._id} value={item._id}>
            {item.name}
          </option>
        ))}
      </select>

      {errors && <div className="alert alert-danger">{errors}</div>}
    </div>
  );
};

export default Select;
