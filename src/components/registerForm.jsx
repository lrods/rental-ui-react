import React from "react";
import Form from "./common/form";
import Joi from "joi-browser";
import { baseUrl } from "../config.json";
import http from "../services/httpService";
import { notify } from "./../utils/notify";

class RegisterForm extends Form {
  state = {
    data: { username: "", password: "", name: "" },
    errors: {}
  };

  schema = {
    username: Joi.string()
      .email()
      .required()
      .label("Username"),
    password: Joi.string()
      .min(6)
      .required()
      .label("Password"),
    name: Joi.string()
      .required()
      .label("Name")
  };

  doSubmit = async () => {
    try {
      const { data } = await http.post(baseUrl + "/users", {
        email: this.state.data.username,
        password: this.state.data.password,
        name: this.state.data.name
      });
      console.log(data);
    } catch (error) {
      notify("error", "Could not register the user");
    }
  };

  render() {
    return (
      <div>
        <h1>Register</h1>

        <form onSubmit={this.handleSubmit}>
          {this.renderInput("username", "Username", "email")}
          {this.renderInput("password", "Password", "password")}
          {this.renderInput("name", "Name", "text")}
          {this.renderButton("Register")}
        </form>
      </div>
    );
  }
}

export default RegisterForm;
