import React from "react";

const ListGroup = props => {
  const { items: genres, onClick, selectedItem: selectedGenre } = props;
  return (
    <ul className="list-group">
      {genres.map(genre => (
        <li
          style={{ cursor: "pointer" }}
          key={genre._id}
          className={
            genre === selectedGenre
              ? "list-group-item active"
              : "list-group-item"
          }
          onClick={() => onClick(genre)}
        >
          {genre.name}
        </li>
      ))}
    </ul>
  );
};

export default ListGroup;
