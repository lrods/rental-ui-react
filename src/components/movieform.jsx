import React from "react";
import Form from "./common/form";
import Joi from "joi-browser";
import http from "../services/httpService";
import { baseUrl } from "../config.json";
import { notify } from "./../utils/notify";

class MovieForm extends Form {
  state = {
    data: {
      _id: "",
      title: "",
      genreId: "",
      numberInStock: 0,
      dailyRentalRate: 0
    },
    genres: [],
    errors: {}
  };

  schema = {
    _id: Joi.string(),
    title: Joi.string()
      .required()
      .label("Title"),
    genreId: Joi.string()
      .required()
      .label("Genre"),
    numberInStock: Joi.number()
      .required()
      .min(0)
      .max(100)
      .label("Number in Stock"),
    dailyRentalRate: Joi.number()
      .required()
      .min(0)
      .max(10)
      .label("Rate")
  };

  doSubmit = () => {
    console.log(this.state.data);
    //saveMovie(this.state.data);
    this.props.history.push("/movies");
  };

  async componentDidMount() {
    const { data } = await http.get(baseUrl + "/genres");
    this.setState({ genres: data });

    if (this.props.match.params.id) {
      try {
        const { data: movie } = await http.get(
          baseUrl + "/movies/" + this.props.match.params.id
        );

        if (movie) {
          const data = { ...this.state.data };
          data._id = movie._id;
          data.title = movie.title;
          data.genreId = movie.genre._id;
          data.numberInStock = movie.numberInStock;
          data.dailyRentalRate = movie.dailyRentalRate;
          this.setState({ data });
        } else {
          this.props.history.replace("/not-found");
        }
      } catch (error) {
        notify("error", "The movie does not exist");
      }
    }
  }

  render() {
    return (
      <div>
        <h1>Movie Form</h1>

        <form onSubmit={this.handleSubmit}>
          {this.renderInput("title", "Title", "text")}
          {this.renderSelect("genreId", "Genre", this.state.genres)}
          {this.renderInput("numberInStock", "Number in Stock", "number")}
          {this.renderInput("dailyRentalRate", "Rate", "number")}
          {this.renderButton("Save")}
        </form>
      </div>
    );
  }
}

export default MovieForm;
