import axios from 'axios'
import { notify } from './../utils/notify';

// axios.interceptors.response.use((response) => {
//     return response
// }, (error) => {
//     console.log(error)
// })

axios.interceptors.response.use(null, error => {
    const expectedError = error.status && error.status >= 400 && error.status < 500

    if (!expectedError) notify('error', 'An unexpected error occured')

    return Promise.reject(error);
})

export default {
    get: axios.get,
    post: axios.post,
    put: axios.put,
    delete: axios.delete
}