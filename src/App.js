import React, { Component } from 'react';
import './App.css';
import Movie from './components/movie'
import NavBar from './components/navbar';
import { Route, Switch, Redirect } from 'react-router-dom'
import Customers from './components/customers';
import Rentals from './components/rentals';
import NotFound from './components/notfound';
import MovieForm from './components/movieform';
import LoginForm from './components/loginForm';
import RegisterForm from './components/registerForm';
import { ToastContainer } from 'react-toastify';
import jwtDecode from 'jwt-decode'
import Logout from './components/logout';

class App extends Component {
  state = {}

  componentDidMount() {
    try {
      const token = localStorage.getItem('token')
      const user = jwtDecode(token)
      this.setState({ user })
    } catch (error) {

    }
  }

  render() {
    return (
      <React.Fragment>
        <ToastContainer />
        <NavBar user={this.state.user} />
        <main className='container'>
          <Switch>
            <Route path='/movies/new' component={MovieForm} />
            <Route path='/movies/:id' component={MovieForm} />
            <Route path='/login' component={LoginForm} />
            <Route path='/logout' component={Logout} />
            <Route path='/register' component={RegisterForm} />
            <Route path='/movies' component={Movie} />
            <Route path='/customers' component={Customers} />
            <Route path='/not-found' component={NotFound} />
            <Route path='/rentals' component={Rentals} />
            <Redirect from='/' exact to='/movies' />
            <Redirect to='/not-found' />
          </Switch>
        </main>
      </React.Fragment>
    );
  }
}

export default App;
