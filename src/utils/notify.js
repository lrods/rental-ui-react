import { toast } from 'react-toastify';

export function notify(type = "info", message) {
    switch (type) {
        case 'error':
            toast.error(message)
            break;
        case 'warning':
            toast.warn(message)
            break;
        case 'success':
            toast.success(message)
            break;
        default:
            toast.info(message)
    }
}
